FROM node:10-alpine as build

WORKDIR /csmoney/chat-frontend

COPY package*.json ./

RUN npm i

COPY ./ ./

RUN npm run build

FROM nginx:1.15.12-alpine

COPY --from=build /csmoney/chat-frontend/build/ /usr/share/nginx/html
