import styled from 'styled-components';

export default styled.div`
  margin: 0px 4px;
  width: 0;
  height: 100%;
`;
