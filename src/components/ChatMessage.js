import React, { Fragment } from 'react';
import imageService from '../api/imageService';

export default ({ username, messageUsername, message, attachment }) => (
  <div>
    <span
      style={{
        color: username === messageUsername ? 'green' : 'black',
      }}
    >
      {messageUsername}:&nbsp;
    </span>
    <span>{message}</span>
    {
      attachment
        ? <Fragment>
          <br/>
          <img src={imageService.getUrl(attachment, { width: 200 })} />
        </Fragment>
        : null
    }
  </div>
)
