import styled from 'styled-components';

export default styled.div`
  margin: 4px 0px;
  width: 100%;
  height: 0;
`;
