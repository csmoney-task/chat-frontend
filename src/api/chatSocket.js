const websocketServerUrl = process.env.NODE_ENV === 'development'
  ? `ws://localhost:8080/?username=`
  : `${window.location.href.replace('http', 'ws')}chat-socket?username=`;

class ChatSocket {
  connect = (username) => {
    this.socket = new WebSocket(websocketServerUrl + username);
    this.socket.onmessage = this.handleMessage.bind(this);
  };

  sendMessage = (message, attachment) => {
    const payload = JSON.stringify({
      type: 'message',
      data: {
        attachment,
        message,
      },
    });

    this.socket.send(payload)
  };

  handleMessage = (message) => {
    const { type, data } = JSON.parse(message.data);

    const handler = this[`on${type}`] || console.warn.bind(console, `unhandled chat event ${type}`, data);

    handler(data);
  };

  onmessage = console.log.bind(console, 'chat socket');
  onerror = console.error.bind(console, 'chat socket');
}

export default new ChatSocket();
