const fileServiceUrl = process.env.NODE_ENV === 'development'
  ? window.location.href
  : `${window.location.href}image-service`;

class ImageService {
  async upload(file) {
    const form = new FormData();

    form.append('file', file);

    const response = await fetch(`${fileServiceUrl}/upload`, {
      method: 'POST',
      body: form,
    }).then(r => r.json());

    return response.imageId;
  }

  getUrl(id, options) {
    const queryString = Object
      .entries({ ...options, id })
      .map(([key, value]) => `${key}=${value}`)
      .join('&');

    return `${fileServiceUrl}/download?${queryString}`;
  }
}

export default new ImageService();
