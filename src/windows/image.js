import React from 'react';
import { Modal } from '@react95/core';
import imageService from '../api/imageService';

export default ({ attachment }) => (
  <Modal
    icon="keys"
    title="Selected image"
    width={200}
    height={230}
    defaultPosition={{
      x: window.innerWidth / 2 + 400,
      y: window.innerHeight / 2 - 200,
    }}
  >
    <img src={imageService.getUrl(attachment, { width: 200, height: 200 })} />
  </Modal>
);
