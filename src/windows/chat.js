import React, { Component } from 'react';
import styled from 'styled-components';
import { Modal, Input, Button } from '@react95/core';
import ChatMessage from '../components/ChatMessage';
import EmptyDivider from '../components/EmptyDivider';
import EmptyVerticalDivider from '../components/EmptyVerticalDivider';

const MessageInput = styled(Input)`
  flex: 1;
  margin-right: 8px;
`;

const WindowContentWrapper = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 24px;
  bottom: 0;
  padding: 8px;
  display: flex;
  flex-direction: column;
`;

const ChatWrapper = styled.div`
  width: 100%;
  box-sizing: border-box;
  border: 2px inset lightgray;
  padding: 8px;
  background-color: white;
  flex: 1;
  overflow-y: scroll;
`;

const MessageInputWrapper = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 8px;
  display: flex;
  justify-content: space-between;
  border: 2px inset lightgray;
`;

export default class ChatWindow extends Component {
  state = {
    message: '',
  };

  handleMessageInput = e => this.setState({
    message: e.target.value,
  });

  handleSendButtonClick = () => {
    const { message } = this.state;

    this.setState({
      message: '',
    });

    this.props.sendChatMessage(message);
  };

  handleAttachImageButtonClick = () => {
    if (this.props.isImageSelected) {
      this.props.resetImage();
    } else {
      this.refs.imageUploader.click();
    }
  };

  handleImageUpload = () => {
    const file = this.refs.imageUploader.files[0];

    if (file) {
      this.props.uploadImage(file);
      this.refs.imageUploader.value = '';
    }
  };

  handleMessageKeyUp = (e) => {
    e.which = e.which || e.keyCode;

    // handle only enter key
    if(e.which === 13) {
      this.handleSendButtonClick();
    }
  };

  scrollChatToBottom = () => {
    this.refs.chat.scrollTop = this.refs.chat.scrollHeight;
  };

  componentDidMount = () => {
    this.refs.imageUploader.onchange = this.handleImageUpload;

    // костылим недостаточный скролл, сорри :)
    setTimeout(() => this.scrollChatToBottom(), 500);
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // if number of messages is changed scroll to bottom
    if (this.props.messages.length !== prevProps.messages.length) {
      this.scrollChatToBottom();
    }
  }

  render = () => {
    const { username, messages, isImageSelected } = this.props;
    const { message } = this.state;

    return (
      <Modal
        icon="mail"
        title="CS.money Chat"
        width={600}
        height={400}
        defaultPosition={{
          x: window.innerWidth / 2 - 300,
          y: window.innerHeight / 2 - 300,
        }}
      >
        <input type="file" ref="imageUploader" style={{display: "none"}}/>

        <WindowContentWrapper>
          <ChatWrapper ref='chat'>
            {
              messages.map(({ username: messageUsername, message, attachment }, i) => (
                <ChatMessage
                  key={i}
                  username={username}
                  messageUsername={messageUsername}
                  message={message}
                  attachment={attachment}
                />
              ))
            }
          </ChatWrapper>

          <EmptyDivider/>

          <MessageInputWrapper>
            <MessageInput
              value={message}
              onInput={this.handleMessageInput}
              onKeyUp={this.handleMessageKeyUp}
            />

            <Button onClick={this.handleAttachImageButtonClick}>
              {
                isImageSelected ? 'Reset image' : 'Attach image'
              }
            </Button>

            <EmptyVerticalDivider/>

            <Button onClick={this.handleSendButtonClick}>Send</Button>
          </MessageInputWrapper>
        </WindowContentWrapper>
      </Modal>
    );
  };
}
