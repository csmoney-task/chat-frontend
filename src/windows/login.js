import React, { Component } from 'react';
import { Modal, Input } from '@react95/core';
import EmptyDivider from '../components/EmptyDivider';

export default class LoginWindow extends Component {
  state = {
    username: '',
  };

  usernameInputHandler = e => this.setState({
    username: e.target.value,
  });

  render = () => {
    const { login } = this.props;
    const { username } = this.state;

    return (
      <Modal
        icon="keys"
        title="Enter Chat"
        width={200}
        height={130}
        defaultPosition={{
          x: window.innerWidth / 2 - 100,
          y: window.innerHeight / 2 - 130,
        }}
        buttons={[
          {
            value: 'Enter!',
            onClick: () => login(username),
          },
        ]}
      >
        <div>Please, enter your username:</div>

        <EmptyDivider />

        <Input
          placeholder='XyLigaN4eG'
          value={username}
          onInput={this.usernameInputHandler}
        />
      </Modal>
    )
  }
}
