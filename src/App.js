import React, { Component, Fragment } from 'react';
import ChatWindow from './windows/chat';
import LoginWindow from './windows/login';
import ImageWindow from './windows/image';
import chatSocket from './api/chatSocket';
import imageService from './api/imageService';

const validate = {
  username: username => /^\w{4,16}$/.test(username),
};

export default class App extends Component {
  state = {
    isLoginWindowOpened: true,
    isChatWindowOpened: false,
    messages: [],
    username: 'no name',
  };

  constructor(props) {
    super(props);

    chatSocket.onwelcome = this.chatWelcomeDataHandler;
    chatSocket.onmessage = this.chatMessageHandler;
    chatSocket.onerror = this.chatErrorHandler;
  }

  login = async (username) => {
    if (validate.username(username)) {
      chatSocket.connect(username);
    } else {
      alert('Invalid username');
    }
  };

  chatWelcomeDataHandler = data => this.setState({
    isLoginWindowOpened: false,
    isChatWindowOpened: true,
    username: data.username,
    messages: data.messages || [],
  });

  chatMessageHandler = message => this.setState(prevState => ({
    messages: [ ...prevState.messages, message ],
  }));

  chatErrorHandler = (e) => {
    console.error(e);
    alert(e.message);
  };

  uploadImage = async (file) => {
    const imageId = await imageService.upload(file);

    this.setState({
      imageId,
    });
  };

  resetImage = () => this.setState({
    imageId: undefined,
  });

  sendChatMessage = (message) => {
    chatSocket.sendMessage(message, this.state.imageId);
    this.resetImage();
  };

  render() {
    const {
      isLoginWindowOpened,
      isChatWindowOpened,
      messages,
      username,
      imageId,
    } = this.state;

    const isImageSelected = !!imageId;

    return (
      <Fragment>
        {
          isLoginWindowOpened
            ? <LoginWindow login={this.login} />
            : null
        }
        {
          isChatWindowOpened
            ? <ChatWindow
                messages={messages}
                username={username}
                resetImage={this.resetImage}
                uploadImage={this.uploadImage}
                isImageSelected={isImageSelected}
                sendChatMessage={this.sendChatMessage}
            />
            : null
        }
        {
          isImageSelected
            ? <ImageWindow attachment={imageId} />
            : null
        }
      </Fragment>
    );
  }
}
